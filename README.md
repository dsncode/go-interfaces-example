# Go interfaces example

This is a very basic example using interfaces usage in go. This example, shows how to create, implement and create flexible code dependencies.

## Intro

This repository has 2 mini projects that leverage go interfaces. One of them is to explain how sorting algorithms could be implemented using the stragegy pattern. the other is to try to explain how, using interfaces, we can mock other interfaces when doing testing. 

### Sorting Algorithms - Overview UML Class Diagram

![img](resources/sort-interfaces.png)

## Mocking Interfaces - Overview UML Class Diagram

![img](resources/mock.png)

## Sorting Algorithms

This repository, has implemented 3 sorting algorithms encapsulated into an interface called [Sorting](sort/sorting.go).
these algoritms implementations can be [found here](sort/sorting.go) and they are:

### BubbleSort
![bubble sort](https://upload.wikimedia.org/wikipedia/commons/c/c8/Bubble-sort-example-300px.gif)
### InsertSort
![insert sort](https://upload.wikimedia.org/wikipedia/commons/0/0f/Insertion-sort-example-300px.gif)

### QuickSort
![quick sort](https://upload.wikimedia.org/wikipedia/commons/6/6a/Sorting_quicksort_anim.gif)

to run the examples, execute:
```go
go test ./... -v
```

This command will display a small benchmark showing all time taken by each algorithm. ex:
```bash
daniel@dsnhvk:~/go/src/gitlab.com/dsncode/go-interfaces-example$ go test ./... -v
?       gitlab.com/dsncode/go-interfaces-example        [no test files]
Initialized: size 100000 view: [770 99 66 473 923 883 658 319 613 110]...=== RUN   TestBubblesort
--- PASS: TestBubblesort (12.59s)
        sorting_test.go:53: bubblesort:  12584 ms
=== RUN   TestInsertsort
--- PASS: TestInsertsort (2.79s)
        sorting_test.go:61: InsertSort:  2788 ms
=== RUN   TestQuicksort
--- PASS: TestQuicksort (0.62s)
        sorting_test.go:69: bubblesort:  78 ms
PASS
ok      gitlab.com/dsncode/go-interfaces-example/sort   15.997s
```

