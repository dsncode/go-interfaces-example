package main

import (
	"fmt"
	"time"

	"gitlab.com/dsncode/go-interfaces-example/generator"
	"gitlab.com/dsncode/go-interfaces-example/sort"
)

func main() {

	sortingAlgoritm := sort.NewBubbleSort()
	numbers := generator.Numbers(100000, 100)

	start := time.Now()
	sortingAlgoritm.Sort(numbers)

	elapsed := time.Since(start)
	fmt.Printf("Bubble sort finished in %f seconds, %d ms\n", elapsed.Seconds(), elapsed/time.Millisecond)
}
