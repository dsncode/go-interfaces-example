package database

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

// User data structure
type User struct {
	Username    string `json:"username"`
	PhoneNumber string `json:"phone_number"`
}

// Database abstraction
type Database interface {
	Save(User) bool
}

// LocalFileDatabase into local storage
type LocalFileDatabase struct {
	writter Writter
}

// Writter write data somewhere
type Writter interface {
	Write(string, []byte) error
}

// LocalWritter write data into local filesystem
type LocalWritter struct {
	location string
}

// Functions

// Save to local storage
func (db *LocalFileDatabase) Save(user User) bool {

	userJSON, err := json.Marshal(user)

	if err != nil {
		log.Println(err)
		return false
	}

	err = db.writter.Write(user.Username+".json", userJSON)
	if err != nil {
		log.Print(err)
		return false
	}

	return true
}

func (wr *LocalWritter) Write(filename string, content []byte) error {
	os.MkdirAll(wr.location, os.ModePerm)
	err := ioutil.WriteFile(wr.location+"/"+filename, content, 0644)
	return err
}

// Constructors

// NewLocalDatabase returns a local storage impl
func NewLocalDatabase(writter Writter) Database {
	return &LocalFileDatabase{
		writter: writter,
	}
}

func NewLocalWritter(location string) Writter {

	return &LocalWritter{
		location: location,
	}
}
