package database

import "testing"

// Test with side effects...
func TestStoreUser(t *testing.T) {
	writter := NewLocalWritter("../data")
	db := NewLocalDatabase(writter)

	user := User{
		Username:    "daniel",
		PhoneNumber: "1-800-20203333",
	}

	success := db.Save(user)

	if success == false {
		t.Fatal("couldn't save demo data")
	}
}

// *****************************************************************************
// No side effects Testing

// Mock writter struct and method
type MockWritter struct {
}

func (wr *MockWritter) Write(location string, data []byte) error {
	return nil
}

func TestMockWritter(t *testing.T) {
	writter := &MockWritter{}
	db := NewLocalDatabase(writter)

	user := User{
		Username:    "daniel",
		PhoneNumber: "1-800-20203333",
	}

	success := db.Save(user)

	if success == false {
		t.Fatal("couldn't save demo data")
	}
}
