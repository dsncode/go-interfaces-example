package sort

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/dsncode/go-interfaces-example/generator"
)

const sampleSize = 100000
const maxNumber = 1000

var numbers []int

func init() {
	numbers = generator.Numbers(sampleSize, maxNumber)
	fmt.Printf("Initialized: array size %d view: %v...\n\n", len(numbers), numbers[0:11])
}

func getNumberCopy() []int {
	numberCopy := make([]int, len(numbers))
	copy(numberCopy, numbers)
	return numberCopy
}

func checkIfSorted(arr []int, t *testing.T) {

	lastNumber := arr[0]
	for _, n := range arr {

		if n < lastNumber {
			t.Fatalf("Numbers are not sorted")
		}
		lastNumber = n
	}

}

func generateTestFunction(sortAlgorithm Sorting, t *testing.T) func(*testing.B) {

	return func(b *testing.B) {
		numCopy := getNumberCopy()
		sortAlgorithm.Sort(numCopy)
		checkIfSorted(numCopy, t)
	}

}

func TestBubblesort(t *testing.T) {

	altorithm := NewBubbleSort()
	trial(altorithm, t)

}

func TestInsertsort(t *testing.T) {

	altorithm := NewInsertSort()
	trial(altorithm, t)

}

func TestQuicksort(t *testing.T) {

	altorithm := NewQuickSort()
	trial(altorithm, t)
}

func trial(altorithm Sorting, t *testing.T) {
	t.Helper()
	algoritmFunc := generateTestFunction(altorithm, t)
	br := testing.Benchmark(algoritmFunc)
	t.Logf("%s in %dms", altorithm.Name(), int64(br.T/time.Millisecond))
}
