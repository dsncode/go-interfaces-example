package sort

// Sorting represents a sorting interface impl
type Sorting interface {
	Sort([]int) []int
	Name() string
}

// BubbleSortImpl bubble sort impl
type BubbleSortImpl struct {
}

// InsertSortImpl insert sort impl
type InsertSortImpl struct {
}

// QuickSortImpl quick sort impl
type QuickSortImpl struct {
}

// NewBubbleSort creates a bubble sort impl
func NewBubbleSort() Sorting {
	return &BubbleSortImpl{}
}

// NewInsertSort creates a Insert sort structrue
func NewInsertSort() Sorting {
	return &InsertSortImpl{}
}

// NewQuickSort creates a Quick sort structure
func NewQuickSort() Sorting {
	return &QuickSortImpl{}
}

// Sort using bubble sort
func (sort *BubbleSortImpl) Sort(items []int) []int {

	L := len(items)

	for i := 0; i < L; i++ {

		for j := 0; j < (L - 1 - i); j++ {
			if items[j] > items[j+1] {
				items[j], items[j+1] = items[j+1], items[j]
			}
		}
	}

	return items
}

// Sort using insert sort algorithm
func (sort *InsertSortImpl) Sort(items []int) []int {
	L := len(items)

	for i := 1; i < L; i++ {

		j := i
		for j > 0 && items[j] < items[j-1] {
			items[j], items[j-1] = items[j-1], items[j]
			j -= 1
		}

	}
	return items
}

// Sort using Quick sort
func (sort *QuickSortImpl) Sort(items []int) []int {
	items = quickSort(items)
	return items
}

// quickSort recursive quick sort algorithm
func quickSort(items []int) []int {
	if len(items) > 1 {
		pivot_index := len(items) / 2
		var smaller_items = []int{}
		var larger_items = []int{}

		for i := range items {
			val := items[i]
			if i != pivot_index {
				if val < items[pivot_index] {
					smaller_items = append(smaller_items, val)
				} else {
					larger_items = append(larger_items, val)
				}
			}
		}

		quickSort(smaller_items)
		quickSort(larger_items)

		var merged []int = append(append(append([]int{}, smaller_items...), []int{items[pivot_index]}...), larger_items...)
		//merged := MergeLists(smaller_items, items[pivot_index], larger_items)

		for j := 0; j < len(items); j++ {
			items[j] = merged[j]
		}

	}
	return items
}

// Name return this impl name
func (sort *BubbleSortImpl) Name() string {
	return "BubbleSort"
}

// Name return this impl name
func (sort *InsertSortImpl) Name() string {
	return "InsertSort"
}

// Name return this impl name
func (sort *QuickSortImpl) Name() string {
	return "QuickSort"
}
