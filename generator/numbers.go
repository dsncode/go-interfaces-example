package generator

import (
	"math/rand"
	"time"
)

// Numbers generate random numbers
func Numbers(size int, limit int) []int {

	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	start := 0
	numbers := make([]int, 0)
	for start < size {

		numbers = append(numbers, r1.Intn(limit))
		start++
	}

	return numbers
}
