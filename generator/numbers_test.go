package generator

import "testing"

func TestNumberGenerator(t *testing.T) {

	size := 1000
	maxNumber := 100
	numbers := Numbers(size, maxNumber)

	if len(numbers) != size {
		t.Fatal("wrong size")
	}

	//check if there is any number bigger than maxNumber

	for _, n := range numbers {
		if n > maxNumber {
			t.Fatalf("a number in the array does not comply maxNumber limitation. found %d. max: %d", n, maxNumber)
		}
	}
}
